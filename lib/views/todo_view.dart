import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../viewmodels/todo_viewmodel.dart';
import '../views/todo_list_view.dart';
import '../views/add_todo_view.dart';
class TodoApp extends StatelessWidget {
  final todoViewModel = TodoViewModel();
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<TodoViewModel>(
        create: (context) => todoViewModel,
        child: MaterialApp(
          title: 'Todo App',
          home: Scaffold(
            appBar: AppBar(
              title: Text('Todo List'),
            ),
            body: Column(
              children: [
                AddTodoView(),
                SizedBox(height: 20),
                Expanded(child: TodoListView()),
              ],
            ),
          ),
        ),
    );
  }
}